import React from "react";
import routerHistory from "./routerHistory"
import {Provider} from "react-redux";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {store} from "./store"
import Main from "./Component/mainPage/main";
import InfoMain from "./Component/infoPage/infoMain";
import LoginPage from "./Component/loginPage/loginPage";
import CartMain from "./Component/cartPage/cartMain";

const Applist = () => {
    return (
        <Provider store={store}>
            <BrowserRouter history={routerHistory}>
                <Routes>
                    <Route path="/" element={<Main />}/>
                    <Route path="/product/:id" element={<InfoMain />}/>
                    <Route path="/account" element={<LoginPage />}/>
                    <Route path="/cart" element={<CartMain />}/>
                </Routes>
            </BrowserRouter>
        </Provider>
    )
}
export default Applist