import React from 'react';
import AppList from "./applist";

function App() {
    return (
        <div>
            <AppList />
        </div>
    );
}

export default App;
