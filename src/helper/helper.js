export const FETCH_CHAIRS = "FETCH_CHAIRS"
export const FETCH_ONE_CHAIR = "FETCH_ONE_CHAIR"
export const GET_REQUEST = "GET_REQUEST"
export const REQUEST_FAILURE = "REQUEST_FAILURE"
export const CHANGE_PRICE_FILTER = "CHANGE_PRICE_FILTER"
export const CHANGE_SORT_FILTER = "CHANGE_SORT_FILTER"

export const SORT_PRICE_ASC = "SORT_PRICE_ASC"
export const SORT_PRICE_DES = "SORT_PRICE_DES"
export const SORT_A_Z = "SORT_A_Z"
export const SORT_Z_A = "SORT_Z_A"

export const CHANGE_SORT_ORDER = "CHANGE_SORT_ORDER"

export const ADD_TO_CART = "ADD_TO_CART"
export const FETCH_CART = "FETCH_CART"
export const CART_DEL_ITEM = "CART_DEL_ITEM"

export const LOG_IN = "LOG_IN"
export const RE_LOGIN = "RE_LOGIN"
export const TOKEN = "TOKEN"

export const CREATING_ORDER = "CREATING_ORDER"
export const CREATE_ORDER_SUCCESS = "CREATE_ORDER_SUCCESS"
export const CREATE_ORDER_FAILED = "CREATE_ORDER_FAILED"

export const ORDER_STORAGE = "ORDER_STORAGE"