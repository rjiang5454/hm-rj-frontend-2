export function showingRating(rating) {
    let arrStar = []
    let m = Math.trunc(rating)
    let f = rating - m
    let n = undefined

    for (let i = 0; i < m; i++) {
        arrStar.push(i)
    }
    if (f > 0.5) {
        arrStar.push(0.5)
        n = 5 - Math.ceil(rating)
    } else {
        n = 5 - Math.floor(rating)
    }
    if (n > 0) {
        for (let t = 0; t < n; t++) {
            arrStar.push(0)
        }
    }

    let ratingStar = arrStar.map(
        (num, index) => {
            if (num === 1){
                return (<i className="fas fa-star" style={{color: "#e22d00"}} key={index}></i>)
            }
            if (num === 0.5){
                return (<i className="fas fa-star-half-alt" style={{color: "#e22d00"}} key={index}></i>)
            }
            else{
                return (<i className="fas fa-star" style={{color: "#e22d00"}} key={index}></i>)
            }
        }
    )
    return ratingStar
}

export function showingPrice (base, prices) {
    let tempPrice = 0
    for (let i = 0; i<prices.length; i++) {
        tempPrice += parseFloat(prices[i])
    }
    let price = (parseFloat(base) + tempPrice).toFixed(2)
    return (
        <div>
            <span>
                {price}
            </span>
        </div>
    )
}