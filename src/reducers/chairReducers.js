import {
    FETCH_CHAIRS,
    GET_REQUEST,
    CHANGE_PRICE_FILTER,
    CHANGE_SORT_FILTER,
    FETCH_ONE_CHAIR
} from "../helper/helper";
import {act} from "react-dom/test-utils";

const initState = {
    chairs: [],
    chair: {},
    isFetching: true,
    priceFilter: null,
    sortOrder: null
}

export default function (state = initState, action) {
    const {type, payload} = action;
    switch (type) {
        case FETCH_CHAIRS:
            return {...state, chairs: payload.data, isFetching: false}
        case FETCH_ONE_CHAIR:
            // console.log("one")
            // console.log(payload.data)
            return {...state, chair: payload.data, isFetching: false}
        case GET_REQUEST:
            return {...state, isFetching: true}
        case CHANGE_PRICE_FILTER:
            return {...state, priceFilter: action.payload}
        case CHANGE_SORT_FILTER:
            return {...state, sortOrder: action.payload}
        default:
            return state
    }
}