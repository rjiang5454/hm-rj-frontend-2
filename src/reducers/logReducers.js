import {GET_REQUEST, LOG_IN} from "../helper/helper";

const initialState = {
    logToken: "",
    msg: "",
    isFetching: true
}

export default function (state = initialState, action) {
    const {type, payload} = action
    switch (type) {
        case LOG_IN:
            if (payload.data) {
                return {...state, logToken: payload.data.token, isFetching: false}
            } else if (payload.msg) {
                return {...state, msg: payload.msg, isFetching: false}
            } else {
                return state
            }
        case GET_REQUEST:
            return {...state, isFetching: true}
        default:
            return state
    }
}