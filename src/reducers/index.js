import chairReducers from "./chairReducers"
import {combineReducers} from "redux";
import cartReducer from "./cartReducers";
import logReducers from "./logReducers";
import orderReducers from "./orderReducers";
import {reducer as formReducer} from "redux-form"

export default combineReducers(
    {
        chairs: chairReducers,
        cart: cartReducer,
        log: logReducers,
        form: formReducer,
        orderReducers
    }
)