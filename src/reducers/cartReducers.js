import {ADD_TO_CART, CART_DEL_ITEM, FETCH_CART} from "../helper/helper";

const initialState = {
    cart: JSON.parse(localStorage.getItem("cartArr")) ? JSON.parse(localStorage.getItem("cartArr")) : []
}

export default function (state = initialState, action) {
    const {type, payload} = action
    switch (type) {
        case ADD_TO_CART:
            const obj = payload
            let cartArr = JSON.parse(localStorage.getItem("cartArr"))
            if (cartArr) {
                let matchId = cartArr.find(ele => ele.chairObj.id === obj.chairObj.id)
                if (!matchId) {
                    cartArr.push(obj)
                } else {
                    let tempArr = []
                    for (let i = 0; i < cartArr.length; i++) {
                        if (cartArr[i].chairObj.id === obj.chairObj.id) {
                            tempArr.push({id: i, obj: cartArr[i]})
                        }
                    }
                    let tempArr1 = []
                    for (let i = 0; i < tempArr.length; i++) {
                        const {checked} = tempArr[i].obj
                        if (JSON.stringify(checked) === JSON.stringify(obj.checked)) {
                            let tempId = tempArr[i].id
                            cartArr[tempId].qty += parseInt(obj.qty)
                            tempArr1.push(1)
                        } else {
                            tempArr1.push(0)
                        }
                    }
                    let tempStatus2 = tempArr1.find(ele => ele === 1)
                    if (!tempStatus2) {
                        cartArr.push(obj)
                    }
                }
                localStorage.setItem("cartArr", JSON.stringify(cartArr))
                return {...state, cart: cartArr}
            } else {
                localStorage.setItem("cartArr", JSON.stringify([obj]))
                return {...state, cart: [obj]}
            }
        case FETCH_CART:
            break
        case CART_DEL_ITEM:
            let tempArr = payload[0]
            let rmIndex = payload[1]
            tempArr = tempArr.filter(
                (arr, index) => {
                    return index !== rmIndex
                }
            )
            if (tempArr.length === 0) {
                localStorage.removeItem("cartArr")
                tempArr = localStorage.getItem("cartaArr")
            }
            else {
                localStorage.setItem("cartArr", JSON.stringify(tempArr))
            }
            return {...state, cart: tempArr}
        default:
            return state
    }
}
