import {CREATE_ORDER_FAILED, CREATE_ORDER_SUCCESS, CREATING_ORDER, RE_LOGIN} from "../helper/helper";

const initialState = {
    orderStatus: false,
    loading: false,
    reLogin: false
}

const orderReducers = (state = initialState, action) => {
    const {type, payload} = action
    switch (type) {
        case CREATING_ORDER:
            return {...state, loading: true}
        case CREATE_ORDER_SUCCESS:
            return {...state, orderStatus: true, loading: false}
        case RE_LOGIN:
            return {...state, reLogin: true}
        default:
            return state
    }
}

export default orderReducers