import {GET_REQUEST, LOG_IN, TOKEN} from "../helper/helper";
import fetching from "../API/fetching";
import routerHistory from "../routerHistory";

export const accountLogin = values => async (dispatch) => {
    dispatch({type: GET_REQUEST})
    return fetching.post("auth/login", values)
        .then((res) => {
            dispatch({
                type: LOG_IN,
                payload: res.data
            })
            localStorage.setItem(TOKEN, res.data.data.token)
            routerHistory.push("/")
        })
        .catch(
            (e => dispatch(
                    {
                        type: LOG_IN,
                        payload: e.response.data,
                        error: true
                    }
                )
            )
        )
}