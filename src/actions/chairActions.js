import {
    FETCH_CHAIRS,
    GET_REQUEST,
    REQUEST_FAILURE,
    CHANGE_PRICE_FILTER,
    CHANGE_SORT_FILTER, CHANGE_SORT_ORDER,
    FETCH_ONE_CHAIR
} from "../helper/helper";
import fetching from "../API/fetching";

export const fetchChairs = () => (dispatch) => {
    dispatch({type: GET_REQUEST});
    return fetching.get("product")
        .then((res) => dispatch({type: FETCH_CHAIRS, payload: res.data}))
        .catch((e => dispatch({type: REQUEST_FAILURE, payload: e, error: true})))
};

export const fetchOneChair = (id) => (dispatch) => {
    return fetching.get(`product/${id}`)
        .then((res) => dispatch({type: FETCH_ONE_CHAIR, payload: res.data}))
        .catch((e => dispatch({type: REQUEST_FAILURE, payload: e, error: true})))
}

export const changePriceFilter = (priceFilter) => (dispatch) => {
    dispatch(
        {
            type: CHANGE_PRICE_FILTER,
            payload: priceFilter
        }
    )
}

export const changeSortFilter = (sortOrder) => (dispatch) => {
    dispatch(
        {
            type: CHANGE_SORT_FILTER,
            payload: sortOrder
        }
    )
}
