import {
    CREATE_ORDER_FAILED,
    CREATE_ORDER_SUCCESS,
    CREATING_ORDER,
    ORDER_STORAGE,
    RE_LOGIN,
    TOKEN
} from "../helper/helper";
import fetching from "../API/fetching";

export const actCreateOrder = (order) => (dispatch) => {
    dispatch({type: CREATING_ORDER})

    let patch = {}
    patch.taxRate = 1.13
    patch.isActive = true
    patch.isDelete = false
    patch.orderItems = []
    order.map(
        (item, index1) => {
            patch.orderItems.push(
                {
                    quantity: parseInt(item.qty),
                    product: item.chairObj.id,
                    profileItems: []
                }
            )
            item.chairObj.profileCategories.map(
                (cat, index2) => {
                    cat.profileItems.map(
                        (profileItem, index3) => {
                            if (profileItem.checked === true) {
                                patch.orderItems[index1].profileItems.push(profileItem.id)
                            }
                        }
                    )
                }
            )
        }
    )

    const token = localStorage.getItem(TOKEN)
    // const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImVtYWlsIjoibWFya3h1QG1hcmsyd2luLmNvbSIsInBhc3N3b3JkIjoiJDJhJDA4JGYvNlpBL1RNTzVjbU55SWwyYmRVYy5zLk5CVTZlOUhtaUxtMjFIVzgwcHIzbTlPTDNZNHRhIiwiaWF0IjoxNjUyNTc0NTE2LCJleHAiOjE2NTI1ODE3MTZ9.joLy-valram64Sy2V3tq2mGoKXWS1NSh4vFGcOdtidE"

    return (
        fetching.post("order", patch, {headers: {"Authorization": `bearer ${token}`}}).then(
            res => {
                if (res.data.code === 8241) {
                    dispatch(
                        {
                            type: RE_LOGIN,
                            payload: res.data
                        }
                    )
                } else {
                    localStorage.setItem(ORDER_STORAGE, JSON.stringify(res.data.data.id))
                    dispatch(
                        {
                            type: CREATE_ORDER_SUCCESS,
                            payload: res
                        }
                    )
                }
            }
        ).catch(
            error => {
                if (error.response.data.code === 8241) {
                    localStorage.removeItem(TOKEN)
                    dispatch(
                        {
                            type: RE_LOGIN,
                            payload: error.response.data
                        }
                    )
                } else {
                    dispatch(
                        {
                            type: CREATE_ORDER_FAILED,
                            payload: error.response
                        }
                    )
                }
            }
        )
    )
}