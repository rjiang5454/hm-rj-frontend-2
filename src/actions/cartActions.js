import {ADD_TO_CART, CART_DEL_ITEM, FETCH_CART} from "../helper/helper";

export const addToCart = (obj) => dispatch => {

    dispatch(
        {
            type: ADD_TO_CART,
            payload: obj
        }
    )
}

export const fetchCart = () => dispatch => {
    dispatch(
        {
            type: FETCH_CART,
            payload: JSON.parse(localStorage.getItem("cartArr"))
        }
    )
}

export const cartDelItem = (index) => dispatch => {
    dispatch (
        {
            type: CART_DEL_ITEM,
            payload: [JSON.parse(localStorage.getItem("cartArr")), index]
        }
    )
}