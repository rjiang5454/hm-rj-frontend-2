import PromoBar from "../mainPage/headerFolder/upperHeader/promoBar";
import ActionBar from "../mainPage/headerFolder/upperHeader/actionBar";
import NavBar from "../mainPage/headerFolder/upperHeader/navBar";
import CartBody from "./cartBody";
import "./cartPage.css"

const CartMain =()=> {
    return(
        <div className="cart-page">
            <PromoBar/>
            <ActionBar/>
            <NavBar/>
            <CartBody/>
        </div>
    )
}

export default CartMain