import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {cartDelItem, fetchCart} from "../../actions/cartActions";
import {Link} from "react-router-dom";
import {useNavigate} from "react-router"
import Breadcrumb from "../mainPage/headerFolder/lowerHeader/breadcrumb";
import {TOKEN} from "../../helper/helper";
import {actCreateOrder} from "../../actions/paymentActions";

const CartBody = () => {

    const cartArr = useSelector(state => state.cart.cart)
    const dispatch = useDispatch()
    const orderStatus = useSelector(state =>state.orderReducers.orderStatus)
    // const history = useHistory()
    const navi = useNavigate()

    useEffect(() => {
        fetchCart()
        console.log(cartArr)
    }, [])

    useEffect(() => {
        fetchCart()
        console.log(cartArr)
    }, [cartArr])

    const onChange = (index, qty) => {

    }

    const checkout = () => {
        if (localStorage.getItem(TOKEN)) {
            actCreateOrder(cartArr)(dispatch)
        } else {
            navi('/account')
        }
    }

    const onClick = (index) => {
        cartDelItem(index)(dispatch)
    }

    const showingCart = (cartArr) => {
        let tempStr = null
        if (cartArr) {
            tempStr = cartArr.map(
                (ele, index) => {
                    let tempObj = ele.chairObj
                    let tempStr2 = null
                    let tempMedia = tempObj.media.split("|")[0]
                    tempStr2 = tempObj.profileCategories.map(
                        (ite, num) => {
                            let tempStr3 = ite.profileItems.map(
                                (e, n) => {
                                    if (ele.checked[num] === n) {
                                        return (
                                            <div className="attribute" key={n}>
                                                {ite.name}: {e.name}
                                            </div>
                                        )
                                    }
                                }
                            )
                            return tempStr3
                        }
                    )
                    const tempArr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    let tempStr3 = null
                    tempStr3 = tempArr.map(
                        (ite, num) => {
                            return (
                                <option value={ite}
                                        key={num}>{ite}</option>
                            )
                        }
                    )
                    return (
                        <tr className="cart-row"
                            key={index}>
                            <td className="item-image">
                                <img src={tempMedia}/>
                            </td>
                            <td className="item-details">
                                <div>
                                    <div className="name">
                                        {tempObj.name}
                                    </div>
                                    {tempStr2}
                                    <ul className="item-actions">
                                        <li className="item-edit">
                                            <div>Edit</div>
                                        </li>
                                        <li className="item-del">
                                            <div onClick={() => onClick(index)}>Remove</div>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td className="item-avail">
                                In Stock
                            </td>
                            <td className="item-price">
                                {ele.price}
                            </td>
                            <td className="item-qty">
                                <select defaultValue={ele.qty}
                                        onChange={(event) => onChange(index, event.target.value)}
                                >
                                    {tempStr3}
                                </select>
                                <i className="fas fa-angle-down"/>
                            </td>
                            <td className="item-total">
                                ${(parseFloat(ele.price) * parseInt(ele.qty)).toFixed(2)}
                            </td>
                        </tr>
                    )
                }
            )
        }
        return tempStr
    }

    return (
        <div className="cart-main">
            <Breadcrumb/>
            <div className="cart-container">
                <div className="cart-header">
                    <h1>My Cart</h1>
                    <div className="header-content">
                        <p className="sub-1">
                            For orders, questions or concerns:
                        </p>
                        <p className="sub-2">
                            Please call
                            <span className="sub-3">
                                888 888 8888
                            </span>
                        </p>
                    </div>
                </div>
                {
                    cartArr && cartArr.length ?
                        <table className="cart-table">
                            <thead>
                            <tr>
                                <th className="cart-table-header">Product Information</th>
                                <th className="cart-table-header"></th>
                                <th className="cart-table-header">Availability</th>
                                <th className="cart-table-header">Price</th>
                                <th className="cart-table-header">Quantity</th>
                                <th className="cart-table-header">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            {showingCart(cartArr)}
                            </tbody>
                        </table>

                        : <div>
                            Empty
                        </div>
                }
                {cartArr === null || cartArr === [] ? "" : <div>
                    {orderStatus === false ?
                        <button onClick={checkout}>Checkout</button>
                        : <div>PayPal</div>}
                </div>
                }
            </div>
        </div>
    )
}

export default CartBody