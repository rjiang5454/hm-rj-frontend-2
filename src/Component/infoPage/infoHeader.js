import InfoBody from "./infoBody/infoBody";
import PromoBar from "../mainPage/headerFolder/upperHeader/promoBar";
import ActionBar from "../mainPage/headerFolder/upperHeader/actionBar";
import NavBar from "../mainPage/headerFolder/upperHeader/navBar";

const InfoHeader = () => {
    const showingPage = () => {
        return (
            <div>
                <PromoBar/>
                <ActionBar/>
                <NavBar/>
                <InfoBody/>
            </div>
        )
    }
    return (
        <div className="info-header">
            {showingPage()}
        </div>
    )
}

export default InfoHeader