import InfoHeader from "./infoHeader";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {useEffect} from "react";
import {fetchOneChair} from "../../actions/chairActions";

const InfoMain =()=> {
    const dispatch = useDispatch()
    const id = useParams().id

    useEffect(
        () => {
            fetchOneChair(id)(dispatch)
        },[]
    )

    return(
        <div>
            <InfoHeader/>
        </div>
    )
}

export default InfoMain