const ProfileItem = ({profileCategories, checked, onClickProfile}) => {
    const showingProfiles = (profile) => {
        let profileBlock = profile?.map(
            (ele, index) => {
                let tempStr = ele.profileItems.map(
                    (name, num) => {
                        return (
                            <li className={checked[index] ===  num ? "selected" : "unselected"}
                                onClick={event => onClickProfile(index, name.price, num)}
                                key={num}>
                                <a>
                                    <span className="swatchanchor-label">
                                        <span className="checked-icon"/>
                                        <i className="far fa-circle"/>
                                        {name.name}
                                    </span>
                                </a>
                            </li>
                        )
                    }
                )
                return (
                    <li className="profiles"
                        key={index}>
                        <div className="profile-label">
                            {ele.name}
                        </div>
                        <div className="profile-items">
                            <ul className="swatch">
                                {tempStr}
                            </ul>
                        </div>
                    </li>
                )
            }
        )
        return profileBlock
    }
    return (
        <div className="product-content-profile">
            <ul>
                {showingProfiles(profileCategories)}
            </ul>
        </div>
    )
}

export default ProfileItem