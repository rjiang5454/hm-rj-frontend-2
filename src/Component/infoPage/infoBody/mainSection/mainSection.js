import ProductImg from "./productImg";
import ProductProfile from "./productProfile";
import "./mainSection.css"

const MainSection =()=> {
    return(
        <div className="main-section">
            <ProductImg/>
            <ProductProfile/>
        </div>
    )
}

export default MainSection