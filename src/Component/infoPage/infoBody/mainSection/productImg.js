import {useSelector} from "react-redux";
import {useState} from "react";

const ProductImg = () => {
    const chairs = useSelector(state => state.chairs)
    const imgStr = chairs.chair.media

    const [mainImgIndex, setMainImgIndex] = useState(0)

    const fetchGallery = (images) => {
        let tempArr = []
        let tempStr = null
        if (images) {
            tempArr = images.replace(" ", "").split("|")
            tempStr = tempArr.map(
                (ele, index) => {
                    return (
                        <li
                            key={index}
                            onClick={() => {
                                setMainImgIndex(index)
                                // console.log(index)
                            }}
                            className="img-ele"
                        >
                            <a
                                className="img-ele-link">
                                <img
                                    src={ele}
                                />
                            </a>
                        </li>
                    )
                }
            )
        }
        return tempStr
    }

    const fetchMainImg = (images) => {
        if (images) {
            let tempArr = images.replace(" ", "").split("|")
            return (
                <a>
                    <img
                        src = {tempArr[mainImgIndex]}
                    />
                </a>
            )
        }
    }

    return (
        <div className="product-img-section">
            <div className="img-inner">
                <div className="img-section-gallery">
                    <div className="gallery-inner">
                        <ul style={{listStyleType: "none", padding: "0"}}>
                            {fetchGallery(imgStr)}
                        </ul>
                        <div className="img-more">
                            More
                        </div>
                    </div>
                </div>
                <div className="img-section-big">
                    <div className="img-big-top">
                        <ul style={{listStyleType: "none"}}
                            className="image-wrap">
                            <li className="img-ele">
                                {fetchMainImg(imgStr)}
                            </li>
                        </ul>
                    </div>
                    <div className="img-big-bottom">
                        <a>
                            <i className="fas fa-info-circle"/>
                            <p>Detail</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductImg