import ProfileItem from "./profileItem";
import ProductCart from "./productCart";
import {useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {showingPrice} from "../../../../helper/funcs";

const ProductProfile = () => {
    const items = {
        rating: [1.4, 2.5, 3.9, 4.5, 5.3, 6.3, 7.6, 8.2, 9.3, 10.5],
        designer: ["designer1, designer2, designer3, designer4, designer5, designer6, designer7" +
        "designer8, designer9, designer10"],
        warranty: ["warranty1$warranty2$warranty3"],
        promoMsg: "free shipping"
    }

    const [checked, setChecked] = useState([])
    const [prices, setPrices] = useState([])
    const [chairObj, setChairObj] = useState({})

    const chairs = useSelector(state => state.chairs)
    // console.log(chairs)

    const onClickProfile = (index, price, num) => {
        let tempClicked = checked
        let tempPrices = prices
        let tempObj = {...chairObj}
        tempClicked[index] = num
        tempPrices[index] = price
        const {profileCategories} = tempObj
        for (let i = 0; i < profileCategories?.length; i++) {
            const {profileItems} = profileCategories[i]
            for (let j = 0; j < profileItems.length; j++) {
                j === num ? profileItems[j].checked = true
                    : profileItems[j].checked = false
            }
        }
        setChecked(tempClicked)
        setChairObj(tempObj)
    }

    useEffect(
        () => {
            const newChair = chairs.chair
            //这里有延迟
            //在这个useEffect的deps里面加了chairs貌似解决了
            // console.log(chairs.chair)
            if (newChair.profileCategories) {
                let tempArr1 = []
                let tempArr2 = []
                const {profileCategories} = newChair
                for (let i = 0; i < profileCategories.length; i++) {
                    const {profileItems} = profileCategories[i]
                    for (let j = 0; j < profileItems.length; j++) {
                        if (profileItems[j].checked) {
                            tempArr1.push(j)
                            tempArr2.push(profileItems[j].price)
                        }
                    }
                }
                setPrices(tempArr2)
                setChecked(tempArr1)
                setChairObj(newChair)
            }
        }, [chairs]
    )

    const {name, price, profileCategories, id} = chairs.chair
    const {rating, designer, warranty, promoMsg} = items

    return (
        <div className="product-profile">
            <h1 className="product-name">{name}</h1>
            <div className="product-content">
                <p className="product-designer"></p>
                <div className="product-content-review">
                    <div className="review-rating">
                        review
                    </div>
                </div>
                <div className="product-content-price">
                    <div className="product-price">
                        {showingPrice(price, prices)}
                    </div>
                </div>
                <ul className="product-content-warranty">
                    <li>w</li>
                    <li>w</li>
                    <li>w</li>
                </ul>
                <div className="product-content-promo">
                    promoMsg
                </div>
                <ProfileItem
                    profileCategories={profileCategories}
                    checked={checked}
                    onClickProfile={onClickProfile}/>
                <div className="product-content-number">
                    number
                    <span>999</span>
                </div>
                <div className="product-content-action">
                    <a>save to wish list</a>
                    <a className="action-print">print</a>
                </div>
                <ProductCart
                    prices={prices}
                    name={name}
                    standard={price}
                    checked={checked}
                    chairObj={chairObj}/>
            </div>
        </div>
    )
}

export default ProductProfile