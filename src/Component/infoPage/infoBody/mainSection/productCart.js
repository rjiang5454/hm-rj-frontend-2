import {showingPrice} from "../../../../helper/funcs";
import {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {addToCart} from "../../../../actions/cartActions";

const ProductCart = ({name, chairObj, standard, checked, prices}) => {
    const [qty, setQty] = useState(1)

    const dispatch = useDispatch()

    const onClick = () => {
        let tempPrice = 0
        for (let i = 0; i < prices.length; i++) {
            tempPrice += parseFloat(prices[i])
        }
        let price = (parseFloat(standard) + tempPrice).toFixed(2)
        let obj = {chairObj, price, qty, checked}
        console.log(obj)
        addToCart(obj)(dispatch)
    }

    return (
        <div className="cart-container">
            <div className="cart-container-wrapper">
                <div className="cart-container-detail">
                    <div className="cart-name">
                        {name}
                    </div>
                </div>
                <div className="cart-action">
                    <div className="cart-info">
                        <div className="cart-info-price">
                            <span>{showingPrice(standard, prices)}</span>
                        </div>
                        <div className="cart-info-avail">
                            <p className="avail-msg">
                                <i className="fas fa-truck fa-flip-horizon"/>
                                In Stock
                            </p>
                        </div>
                    </div>
                    <div className="cart-add">
                        <div className="cart-add-qty">
                            <input type="text" min="1" value={qty} maxLength="3"
                                   onChange={event => setQty(event.target.value)}/>
                            <button className="cart-add-btn"
                                    onClick={event => onClick()}>
                                Add To Cart
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductCart