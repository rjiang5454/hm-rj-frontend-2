import Breadcrumb from "../../mainPage/headerFolder/lowerHeader/breadcrumb";
import MainContent from "./mainContent";

const InfoBody =()=> {
    return(
        <div className="info-main">
            <Breadcrumb/>
            <MainContent/>
        </div>
    )
}

export default InfoBody