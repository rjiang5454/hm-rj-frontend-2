import DetailSection from "./detailSection/detailSection";
import MainSection from "./mainSection/mainSection";

const mainContent =()=> {
    return(
        <div className="main__content">
            <MainSection/>
            <DetailSection/>
        </div>
    )
}

export default mainContent