import LeftCol from "./leftCol";
import RightCol from "./rightCol";
import {useEffect, useState} from "react";
import {TOKEN} from "../../helper/helper";
import {useSelector} from "react-redux";

const LogForm =()=> {
    const [isToken, setTokenStatus] = useState(false)
    const tokenStatus = useSelector(state => state.log.logToken)

    useEffect(
        () => {
            if (localStorage.getItem(TOKEN)){
                setTokenStatus(true)
            }
        },[]
    )

    useEffect(
        () => {
            if (localStorage.getItem(TOKEN)){
                setTokenStatus(true)
            }
        },[tokenStatus]
    )

    return (
        !isToken ?
        <div>
            <LeftCol/>
            <RightCol/>
        </div>
            : <div>
            Thanks
            </div>
    )
}

export default LogForm