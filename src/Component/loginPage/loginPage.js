import PromoBar from "../mainPage/headerFolder/upperHeader/promoBar";
import ActionBar from "../mainPage/headerFolder/upperHeader/actionBar";
import NavBar from "../mainPage/headerFolder/upperHeader/navBar";
import LogMain from "./logMain";

const LoginPage =()=> {
    return (
        <div>
            <PromoBar/>
            <ActionBar/>
            <NavBar/>
            <LogMain/>
        </div>
    )
}

export default LoginPage