import React from "react";
import {Field, reduxForm} from "redux-form";
import {useDispatch, useSelector} from "react-redux";
import {accountLogin} from "../../actions/logActions";

const validate = () => {

}

const LeftCol = ({handleSubmit, reset}) => {
    const msg = useSelector(state => state.log.msg)
    const dispatch = useDispatch()

    const renderInput = ({input, label, type, meta: {touched, error}}) => (
        <div>
            <label htmlFor={label}>{`${label}*`}</label>
            {console.log(input)}
            <div>
                <input type={type} {...input}/>
            </div>
        </div>
    )

    const loginError = (msg) => {
        if (msg) {
            return (
                <div className="error-msg">
                    <p>Error</p>
                </div>
            )
        } else {
            return false
        }
    }

    const onSubmit = (formValue) => {
        accountLogin(formValue)(dispatch)
        reset()
    }

    return (
        <div className="col-1">
            <div className="log-box">
                <legend className="legend-heading">
                    <h2>Sign In</h2>
                    <div className="log-required">
                        * Required
                    </div>
                </legend>
                {loginError(msg)}
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="left-form">
                        <div className="form-row">
                            <Field type="text" name="email" label="email" component={renderInput}/>
                        </div>
                        <div className="form-row">
                            <Field type="text" name="password" label="password" component={renderInput}/>
                        </div>
                        <div className="form-row">
                            <button type="submit">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

const leftForm = reduxForm(
    {
        form: "accountform",
    }
)(LeftCol)

export default leftForm