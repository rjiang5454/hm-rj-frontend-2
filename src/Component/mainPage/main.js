import React, {useEffect} from "react";
import {useDispatch} from "react-redux";
import {fetchChairs} from "../../actions/chairActions";
import Header from "./headerFolder/header";

const Main =()=> {
    const dispatch = useDispatch()

    useEffect( ()=>{
        fetchChairs()(dispatch)
        }, []
    )

    return (
        <div>
            <Header />
        </div>
    )
}

export default Main