import {CategoryHeader} from "./categoryHeader";
import FilterBar from "./productList/filterBar/filterBar";
import ProductDisplay from "./productList/productDisplay/productDisplay";
import {useState} from "react";

const MainContent =()=>{
    const [smallTileClicked, setSmallTileClicked] = useState(true)
    const handleClick =()=> {
        setSmallTileClicked(!smallTileClicked)
    }
    return (
        <div>
            <CategoryHeader />
            <div>
                <FilterBar />
                <ProductDisplay smallTileClicked = {smallTileClicked}/>
            </div>
        </div>
    )
}

export default MainContent