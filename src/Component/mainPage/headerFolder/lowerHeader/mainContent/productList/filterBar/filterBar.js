import LeftFunctionFilter from "./leftFunctionFilter";
import {RightDisplayFilter} from "./rightDisplayFilter";

const FilterBar = () => {
    return (
        <div>
            <LeftFunctionFilter/>
            <RightDisplayFilter/>
        </div>

    )
}

export default FilterBar