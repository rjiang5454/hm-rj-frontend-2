import {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {changePriceFilter, changeSortFilter} from "../../../../../../../actions/chairActions";
import {SORT_A_Z, SORT_PRICE_ASC, SORT_PRICE_DES, SORT_Z_A} from "../../../../../../../helper/helper";

const LeftFunctionFilter =()=>{
    const [displayPrice, setDisplayPrice] = useState(false)
    const [displayMaterial, setDisplayMaterial] = useState(false)
    const [displayFilter, setDisplayFilter] = useState(false)
    const [priceSelected, setPriceSelected] = useState([])

    const dispatch = useDispatch()

    useEffect(() => {
        changePriceFilter(priceSelected)(dispatch)
    }, [priceSelected])

    const priceFilterHandleClick =()=> {
        if (displayMaterial) {
            setDisplayMaterial(!displayMaterial)
            setDisplayPrice((!displayPrice))
        }
        else {
            setDisplayMaterial(!displayMaterial)
        }
    }

    const priceRangeHandleClick =(selectedPriceIndex)=> {
        const index = priceSelected.indexOf(selectedPriceIndex)
        if(index !== -1){
            let temp = [...priceSelected]
            temp.splice(index, 1)
            setPriceSelected(temp)
        }
        else
            setPriceSelected([...priceSelected, selectedPriceIndex])
    }

    const sortFilterHandleClick =()=> {
        setDisplayFilter(!displayFilter)
    }

    const materialFilterHandleClick =()=> {
        if (displayPrice === true) {
            setDisplayMaterial(!displayMaterial)
            setDisplayPrice((!displayPrice))
        }
        else {
            setDisplayMaterial(!displayMaterial)
        }
    }

    const displayFilterHandler =(e)=> {
        switch (e.target.value) {
            case "0":
                changeSortFilter(SORT_PRICE_DES)(dispatch)
                break
            case "1":
                changeSortFilter(SORT_PRICE_ASC)(dispatch)
                break
            case "2":
                changeSortFilter(SORT_A_Z)(dispatch)
                break
            case "3":
                changeSortFilter(SORT_Z_A)(dispatch)
                break
            default:
                changeSortFilter(null)(dispatch)
                break
        }
    }

    return (
        <div className="filter-bar__left">
            <div className="left-desktop">
                <div className="filter-ele">
                    <h3 onClick={priceFilterHandleClick}>
                        <span>Price</span>
                    </h3>
                    <ul style={{display: displayPrice ? "block" : "none"}}>
                        <li>
                            <input type="checkbox" onClick={() => priceRangeHandleClick(1)}/>
                            <div>0-500</div>
                        </li>
                        <li>
                            <input type="checkbox" onClick={() => priceRangeHandleClick(2)}/>
                            <div>500-1000</div>
                        </li>
                        <li>
                            <input type="checkbox" onClick={() => priceRangeHandleClick(3)}/>
                            <div>1000-1500</div>
                        </li>
                        <li>
                            <input type="checkbox" onClick={() => priceRangeHandleClick(4)}/>
                            <div>1500-2000</div>
                        </li>
                        <li>
                            <input type="checkbox" onClick={() => priceRangeHandleClick(5)}/>
                            <div>2000-2500</div>
                        </li>
                        <li>
                            <input type="checkbox" onClick={() => priceRangeHandleClick(6)}/>
                            <div>above 2500</div>
                        </li>
                    </ul>
                </div>
                <div className="filter-ele">
                    <h3 onClick={materialFilterHandleClick}>
                        <span>Material</span>
                    </h3>
                    <ul style={{display: displayMaterial ? "block" : "none"}}>
                        <li>A</li>
                        <li>B</li>
                        <li>C</li>
                        <li>D</li>
                    </ul>
                </div>
            </div>
            <div className="filter-bar__sort">
                <label htmlFor="sort-opt">Sort By:</label>
                <div className="select-style">
                    <select id="sort-opt" onChange={(e) => displayFilterHandler(e)}>
                        <option>Featured Product</option>
                        <option value={0}>Price: High to Low</option>
                        <option value={1}>Price: Low to High</option>
                        <option value={2}>Name: A to Z</option>
                        <option value={3}>Name: Z to A</option>
                    </select>
                    <span>
                        <i className="fas fa-sort-down"/>
                    </span>
                </div>
            </div>
        </div>
    )
}

export default LeftFunctionFilter