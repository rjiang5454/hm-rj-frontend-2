import ProductVar from "./productVar";
import {Link} from "react-router-dom";

const ProductContent = ({name, swatch, standard, number}) => {
    return (
        <div className="product-content">
            <div className="product-content-name">
                <Link to={`product/${number}`}>
                    <h3>{name}</h3>
                </Link>
            </div>
            <div className="product-content-price">
                <h3>{standard}</h3>
            </div>
            <ProductVar swatch={swatch}/>
        </div>
    )
}

export default ProductContent