import {Link} from "react-router-dom";

const ProductImage = ({imageStr, number}) => {
    const id = number
    let mediaArr = imageStr ? imageStr.split("|") : []

    return (
        <div className="product-img">
            <Link className="img-link" to={`product/${id}`}>
                <img className="product-img__cover"
                    src={mediaArr[0].trim()} />
            </Link>
        </div>
    )
}

export default ProductImage