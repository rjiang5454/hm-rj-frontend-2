const ProductVar = ({swatch}) => {
    const showVar =(varSource)=> {
        let varImg = null
        varImg =  varSource[0].profileItems.map((ele, index) => {
            return (
                <li key={index}>
                    <a className="swatch">
                        <img src={ele.media}/>
                    </a>
                </li>
            )
        })
        return varImg
    }
    return (
        <div className="product-content-swatch">
            <ul>
                {showVar(swatch)}
            </ul>
        </div>
    )
}

export default ProductVar