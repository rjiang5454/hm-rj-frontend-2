import ProductImage from "./productImage";
import ProductContent from "./productContent";
import {useSelector} from "react-redux";
import React, {useEffect, useState} from "react";
import {SORT_PRICE_ASC} from "../../../../../../../helper/helper";

const ProductDisplay = ({smallTileClicked}) => {
    const chairs = useSelector(state => state.chairs.chairs)
    const priceFilter = useSelector(state => state.chairs.priceFilter)
    const sortOrder = useSelector(state => state.chairs.sortOrder)

    const [filteredChairs, setFilteredChairs] = useState([])

    useEffect(() => {
        chairDisplay()
    },[chairs, priceFilter, sortOrder])

    const chairDisplay =()=> {
        let selectedChairs = []

        if (chairs) {
            chairs.forEach((chair) => {
                if (priceFilter && priceFilter.length !== 0){
                    for (let i = 0; i < priceFilter.length; i++){
                        if (parseInt(chair.price) <= parseInt(priceFilter[i]) * 500 &&
                            parseInt(chair.price) >= (parseInt(priceFilter[i]) - 1) * 500) {
                            selectedChairs.push(chair)
                            break
                        }
                    }
                }else{
                    selectedChairs = [...chairs]
                }
            })
        }

        if (sortOrder) {
            switch (sortOrder) {
                case SORT_PRICE_ASC:
                    selectedChairs.sort(priceAscFilter)
                    break
            }
        }

        setFilteredChairs(selectedChairs)
    }

    const priceAscFilter =(a,b)=> {
        if(parseInt(a.price) < parseInt(b.price)){
            return -1
        }
        if(parseInt(a.price) > parseInt(b.price)){
            return 1
        }
        return 0
    }

    let chairsForDisplay = filteredChairs.map((ite, index) => {
            return (
                <li className="grid-tile" key={index}>
                    <div className="product-tile">
                        <ProductImage imageStr={ite.media} number={ite.id}/>
                        <ProductContent name={ite.name} standard={ite.price} swatch={ite.profileCategories} number={ite.id}/>
                    </div>
                </li>
            )
        }
    )

    return (
        <div className="search-product-content">
            <ul id="search-product-items">
                {chairsForDisplay}
            </ul>
        </div>
    )
}

export default ProductDisplay