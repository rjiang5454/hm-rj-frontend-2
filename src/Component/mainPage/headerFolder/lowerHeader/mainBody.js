import React from "react";
import Breadcrumb from "./breadcrumb";
import MainContent from "./mainContent/mainContent";
import "./mainBody.css";

export const MainBody =()=>{
    return (
        <div>
            <Breadcrumb />
            <MainContent />
        </div>
    )
}