import {Component} from "react";
import {MainBody} from "./lowerHeader/mainBody";
import PromoBar from "./upperHeader/promoBar";
import NavBar from "./upperHeader/navBar";
import MobileHeader from "./upperHeader/mobileHeader";
import ActionBar from "./upperHeader/actionBar";

class Header extends Component {
    initMainPage = () => {
        return (
            <div>
                <PromoBar/>
                <ActionBar/>
                <NavBar/>
                <MainBody/>
            </div>
        )
    }

    render() {
        return (
            <div>
                {this.initMainPage()}
            </div>
        )
    }
}

export default Header