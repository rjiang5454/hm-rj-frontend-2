import {Link} from "react-router-dom";
import "./upperHeader.css"
import {useState} from "react";
import logForm from "../../../loginPage/logForm";
import {TOKEN} from "../../../../helper/helper";

const ActionBar = () => {

    const [opacity, setOpacity] = useState(0)
    const [visibility, setVisibilty] = useState("hidden")
    const [storeStatus, setStoreStatus] = useState(true)
    const [contactStatus, setContactStatus] = useState(false)
    const [tokenStatus, setTokenStatus] = useState(false)

    const handleStoreStatus = () => {
        if (storeStatus) {
            setStoreStatus(storeStatus)
            setContactStatus(contactStatus)
        } else {
            setStoreStatus(!storeStatus)
            setContactStatus(!contactStatus)
        }
    }

    const handleContactStatus = () => {
        if (contactStatus) {
            setStoreStatus(storeStatus)
            setContactStatus(contactStatus)
        } else {
            setStoreStatus(!storeStatus)
            setContactStatus(!contactStatus)
        }
    }

    const handleMouseEnter = () => {
        setOpacity(1)
        setVisibilty("visible")
        if (localStorage.getItem(TOKEN)) {
            setTokenStatus(true)
        }
    }

    const handleMouseleave = () => {
        setOpacity(0)
        setVisibilty("hidden")
    }

    const logOut = () => {
        localStorage.removeItem(TOKEN)
        setTokenStatus(false)
    }

    return (
        <div className="action-bar">
            <div className="action-wrapper">
                <div className="action-wrapper-inner">
                    <ul className="action-left">
                        <li className={storeStatus ? "active" : ""} onClick={handleStoreStatus}>
                            Store
                        </li>
                        <li className={contactStatus ? "active" : ""} onClick={handleContactStatus}>
                            Contact
                        </li>
                    </ul>
                    <ul className="action-center">
                        <li>
                            Customer Service
                        </li>
                        <li>
                            888 888 8888
                        </li>
                    </ul>
                    <ul className="action-right">
                        <li className="right__account" onMouseLeave={handleMouseleave}>
                            <Link to={"/account"}>
                                <span onMouseEnter={handleMouseEnter}>
                                    My Account
                                </span>
                                <span>
                                    <i className="fas fa-user"/>
                                </span>
                            </Link>
                            {
                                tokenStatus ? <div>
                                        <div className="right__account-userpanel"
                                             style={{opacity: opacity, visibility: visibility}}>
                                            {/*<Link to="/account">Logout</Link>*/}
                                            <div onClick={logOut}
                                            >Log Out</div>
                                        </div>
                                    </div>
                                    : <div className="right__account-userpanel"
                                           style={{opacity: opacity, visibility: visibility}}>
                                        <Link to="/account">Log In</Link>
                                        <Link to="/account">Register</Link>
                                    </div>
                            }

                        </li>
                        <li className="right__cart">
                            <Link to={"/cart"}>
                                <span>
                                    Cart
                                </span>
                                <span>
                                    <i className="fas fa-shopping-cart"/>
                                </span>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default ActionBar