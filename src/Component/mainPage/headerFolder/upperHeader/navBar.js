const NavBar =()=>{
    return (
        <div className="nav-bar">
            <div className="nav-wrapper">
                <div className="nav-wrapper__inner">
                    <h2 className="nav-logo">
                        logo
                    </h2>
                    <nav id="navigation">
                        <ul className="category lev-1">
                            <li className="menu">
                                <a>
                                    <span>Office</span>
                                </a>
                            </li>
                            <li className="menu">
                                <a>
                                    <span>Living</span>
                                </a>
                            </li>
                            <li className="menu">
                                <a>
                                    <span>Dining</span>
                                </a>
                            </li>
                            <li className="menu">
                                <a>
                                    <span>Bedroom</span>
                                </a>
                            </li>
                            <li className="menu">
                                <a>
                                    <span>Outdoor</span>
                                </a>
                            </li>
                            <li className="menu">
                                <a>
                                    <span>Lighting</span>
                                </a>
                            </li>
                            <li className="menu">
                                <a>
                                    <span>Accessories</span>
                                </a>
                            </li>
                            <li className="menu">
                                <a>
                                    <span>Gaming</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div className="header-search">
                        <input className="header-search__input"/>
                        <i className="fas fa-search header-search__button"/>
                        {/*<i className="fas fa-times-circle"/>*/}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NavBar