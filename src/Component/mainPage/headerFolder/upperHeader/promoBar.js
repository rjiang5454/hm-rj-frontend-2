import {useState} from "react";

const PromoBar =()=>{
    const [displayStatus, setDisplayStatus] = useState("block")

    const onClickDisplay =()=> {
        setDisplayStatus("none")
    }
    return (
        <div style={{display: displayStatus}}>
            <div>
                <span>
                    Enjoy Free Shopping on Office Chairs + 0% Financing Available
                </span>
            </div>
            <div>
                <span onClick={onClickDisplay}>
                    <i className="fas fa-times" />
                </span>
            </div>
        </div>
    )
}

export default PromoBar